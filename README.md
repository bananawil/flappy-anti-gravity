# flappy-anti-gravity

This is a single-player android game with rules similar to the famous Flappy Bird game, except that the bird is not falling by gravity like in Flappy Bird, but the "Olaf ball" is floating by anti-gravity.

The game should run well on any android device having SDK v16 (Android 4.1) or above. A bunch of parameters can be tuned for setting the game in different difficulties and for further development of the game.

Any questions, suggestions or bug reports please contact me at eatmutho123@gmail.com.