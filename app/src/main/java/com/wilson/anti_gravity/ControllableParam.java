/* Holds all parameters that can be tuned for different game difficulties */

package com.wilson.anti_gravity;

public class ControllableParam {

    /* * * * * * * * * * * * * * * * * * *
     *      Parameters for Columns       *
     * * * * * * * * * * * * * * * * * * */

    /* width of a column, default to be 1/6 of width of screen */
    public static final float COLUMN_WIDTH = FixedParam.SCREEN_WIDTH * (float)(1./6.);

    /* minimum offset of the bottom of top column from top of screen, default to be 1/12 of screen height */
    public static final float TOP_COLUMN_MIN_BOTTOM_OFFSET = FixedParam.SCREEN_HEIGHT * (float)(1./12.);

    /* maximum offset of the top of bottom column from top of screen, default to be 11/12 of screen height */
    public static final float BOTTOM_COLUMN_MAX_TOP_OFFSET = FixedParam.SCREEN_HEIGHT * (float)(11./12.);

    /* spacing between top and bottom columns, which must be a number smaller than the difference
       between @TOP_COLUMN_MIN_BOTTOM_OFFSET and @BOTTOM_COLUMN_MAX_TOP_OFFSET, default to be
       1/4 of screen height */
    public static final float VERTICAL_COLUMN_SPACING = FixedParam.SCREEN_HEIGHT * (float)(1./4.);

    /* spacing between the left positions of adjacent columns, default to be 3/4 of screen width */
    public static final float HORIZONTAL_COLUMN_SPACING = FixedParam.SCREEN_WIDTH * (float)(3./4.);


    /* speed of moving column, default to be 1/144 of width of screen per animation frame */
    public static final float COLUMN_MOVING_SPEED = FixedParam.SCREEN_WIDTH * (float)(1./144.);


    /* * * * * * * * * * * * * * * * * * *
     *      Parameters for Balls         *
     * * * * * * * * * * * * * * * * * * */

    /* units of time passed for the trajectory of the ball per one frame, default to be 10 */
    public static final int BALL_MOVING_SPEED = 10;

    /* width of ball, default to be 1/7 of screen width */
    public static final float BALL_WIDTH = FixedParam.SCREEN_WIDTH * (float)(1./7.);

    /* height of ball, default to be 1/7 of screen width */
    public static final float BALL_HEIGHT = FixedParam.SCREEN_WIDTH * (float)(1./7.);

    /* initial left position of ball, default to be constant and 1/7 of screen width */
    public static final float INIT_BALL_LEFT = FixedParam.SCREEN_WIDTH * (float)(1./7.);

    /* initial top position of ball, default to be 1/2 of screen height */
    public static final float INIT_BALL_TOP = FixedParam.SCREEN_HEIGHT * (float)(1./2.);

    /* trough of the trajectory of ball measured from Ball@topOffSet, default to be 1/2 of screen height*/
    public static final float BALL_TROUGH = FixedParam.SCREEN_HEIGHT * (float)(1./2.);

    /* time parameter reset for the ball trajectory after touching screen */
    public static final int BALL_JUMP_TIME = 330;


    /* * * * * * * * * * * * * * * * * * *
     *         Other Parameters          *
     * * * * * * * * * * * * * * * * * * */

    /* actual duration of one frame of animation for columns and balls (in ms), default value is 5 ms */
    public static final int ANIMATION_FRAME_DURATION = 5;

}






















