/* This class is a wrapper for the view representing a ball including get-set methods for its
   top and left positions, etc. @nextLeft represents the left position of the ball in the next
   frame (Column is set to move at constant speed by default).

   ColumnPair is a static sub-class of this class which groups a pair of up and bottom columns
   together for easier manipulation of columns (since the up and bottom of columns will share the
   same background color and left position, etc. by default). @index is a unique number key
   representing every pair (every pair will loop over again during the game) */

package com.wilson.anti_gravity;

import android.view.View;
import android.view.ViewGroup;

public class Column {

    public static class ColumnPair {
        private Column topColumn;
        private Column bottomColumn;
        private int index;
        private int backgroundColor;

        public ColumnPair(Column topColumn, Column bottomColumn) {
            this.topColumn = topColumn;
            this.bottomColumn = bottomColumn;
        }

        public Column getTopColumn() {
            return this.topColumn;
        }

        public Column getBottomColumn() {
            return this.bottomColumn;
        }

        public int getIndex() {
            return this.index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public void setHeightAndWidth(int height, int width) {
            this.topColumn.setHeightAndWidth(height, width);
            this.bottomColumn.setHeightAndWidth(height, width);
        }

        public void setVisibility(boolean visible) {
            this.topColumn.setVisibility(visible);
            this.bottomColumn.setVisibility(visible);
        }

        public int getBackgroundColor() {
            return this.backgroundColor;
        }

        public void setBackgroundColor(int backgroundColor) {
            this.topColumn.setBackgroundColor(backgroundColor);
            this.bottomColumn.setBackgroundColor(backgroundColor);
            this.backgroundColor = backgroundColor;
        }
    }

	private View columnView;
	private float top;
	private float left;
	private float nextLeft;
	
	
	public Column(View columnView){
		this.columnView = columnView;
		this.top = 0;
		this.left = 0;
		this.nextLeft = 0;
	}
	
	public float getTop() {
	    return this.top;
    }

    public void setTop(float top) {
	    this.top = top;
	    this.columnView.setY(top);
    }

    public float getLeft() {
	    return this.left;
    }

    /* To set the left position, always apply this method then apply setLeftFromNextLeft(). This is
   to make sure the left position is always set by using @nextLeft after initialization, so that
   the left position can always have same correspondence with Ball@trajectoryTime. */
    public void setNextLeft(float nextLeft) {
	    this.nextLeft = nextLeft;
    }

    public void setLeftFromNextLeft() {
	    this.left = this.nextLeft;
        this.columnView.setX(this.left);
    }

    private void setHeightAndWidth(int height, int width) {
        ViewGroup.LayoutParams params = this.columnView.getLayoutParams();
        params.height = height;
        params.width = width;
        this.columnView.setLayoutParams(params);
    }

    private void setVisibility(boolean visible) {
        if (visible)
            this.columnView.setVisibility(View.VISIBLE);
        else
            this.columnView.setVisibility(View.INVISIBLE);
    }

    private void setBackgroundColor(int backgroundColor) {
	    this.columnView.setBackgroundColor(backgroundColor);
    }



	
}
