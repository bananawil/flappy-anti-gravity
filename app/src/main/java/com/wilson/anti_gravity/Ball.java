/* This class is a wrapper for the view representing a ball including get-set methods for its top
   and left positions, etc. @topOffSet represents the offset of "ground" (but up-side-down) of
   trajectory from the top of screen; @nextTop represents the top position of the ball in the next
   frame; @trajectoryTime represents the time parameter of ball on its trajectory, and by default,
   0 and 1000 are two moments when the ball passes through the "ground", while the ball is at its
   trough when @trajectoryTime = 500. */


package com.wilson.anti_gravity;

import android.view.View;
import android.view.ViewGroup;

public class Ball {

	private View ballView;
	private float top;
	private float left;
	private float topOffSet;
	private float nextTop;
	private int trajectoryTime;
	
	
	public Ball(View ballView, float initTop, float initLeft, int trajectoryTime){
		this.ballView = ballView;
		this.top = initTop;
		this.left = initLeft;
		this.topOffSet = 0;
		this.trajectoryTime = trajectoryTime;
		ballView.setX(initLeft);
		ballView.setY(initTop);
	}

	public float getTop() {
		return this.top;
	}

	public float getNextTop() {
		return this.nextTop;
	}

	/* To set the top position, always apply this method then apply setTopFromNextTop(). This is
	   to make sure the top position is always set by using @nextTop after initialization, so that
	   the top position can always have same correspondence with @trajectoryTime. */
	public void setNextTop(float nextTop) {
		this.nextTop = nextTop;
	}

	public void setTopFromNextTop() {
		this.top = this.nextTop;
        this.ballView.setY(this.top);
	}

	public float getTopOffSet() {
	    return this.topOffSet;
    }

    public void setTopOffSet(float topOffSet) {
	    this.topOffSet = topOffSet;
    }

    public float getLeft() {
	    return this.left;
    }

	public int getTrajectoryTime() {
	    return this.trajectoryTime;
    }

    public void setTrajectoryTime(int trajectoryTime) {
	    this.trajectoryTime = trajectoryTime;
    }

    public void setHeightAndWidth(int height, int width) {
        ViewGroup.LayoutParams params = this.ballView.getLayoutParams();
        params.height = height;
        params.width = width;
        this.ballView.setLayoutParams(params);
    }

    public void setVisibility(boolean visible) {
	    if (visible)
	        this.ballView.setVisibility(View.VISIBLE);
	    else
	        this.ballView.setVisibility(View.INVISIBLE);
    }

    /* bring the ball view to the most front of screen */
    public void bringToFront() {
	    this.ballView.bringToFront();
    }

}
