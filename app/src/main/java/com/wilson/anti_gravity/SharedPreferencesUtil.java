/* Provides static methods for get-setting scores from SharedPreferences (SP) storage */

package com.wilson.anti_gravity;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtil {
	

	private static final String SHARED_PREFERENCES_SCORE = "score"; // Name of SP storage
    private static final String INT_HIGHEST_SCORE = "highest_score"; // Name of highest score integer in SP storage


    public static int getSharedPreferencesHighestScore(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARED_PREFERENCES_SCORE, Context.MODE_PRIVATE);
        return sp.getInt(INT_HIGHEST_SCORE, 0); // return highest score integer in SP storage, or 0 for initialization
    }

    public static void setSharedPreferencesHighestScore(Context context, int highestScore) {
        SharedPreferences sp = context.getSharedPreferences(
                SHARED_PREFERENCES_SCORE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(INT_HIGHEST_SCORE, highestScore);
        editor.apply();
    }
}
