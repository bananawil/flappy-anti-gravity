/* This class is a sub-class of Application class, and is holding a "singleton" static context
   (by instantiating itself), which can be retrieved by getContext(). */

package com.wilson.anti_gravity;

import android.app.Application;
import android.content.Context;

public class MyApp extends Application{

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext(){
        return mContext;
    }
}
