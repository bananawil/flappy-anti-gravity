/* Holds all fixed parameters */

package com.wilson.anti_gravity;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

public class FixedParam {

    /* Retrieve height and width of display screen (without status bar and action bar) */
    public static final int SCREEN_HEIGHT;
    public static final int SCREEN_WIDTH;

    static {
        Display display = ((WindowManager) MyApp.getContext().getSystemService(
                Context.WINDOW_SERVICE)).getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        SCREEN_HEIGHT = size.y;
        SCREEN_WIDTH = size.x;
    }

    /* Message value for utility handler */
    public static final int BALL_CRASH = 100;
    public static final int ADD_SCORE = 200;
    public static final int START_BUTTON_VISIBLE = 300;

}
