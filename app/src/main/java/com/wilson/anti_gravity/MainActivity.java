/* Main Activity class for the whole game to hold */

package com.wilson.anti_gravity;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.view.View.OnTouchListener;
import android.os.Handler;
import android.os.Message;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnTouchListener {

    // number of column pairs required, calculated from screen width and spacing between the left
    // positions of adjacent columns
    private static final int NUMBER_OF_COLUMN_PAIRS =  (int)(FixedParam.SCREEN_WIDTH /
            ControllableParam.HORIZONTAL_COLUMN_SPACING) + 2;

    // holds the ball object
    private Ball ball;
    // holds all pairs of up and bottom columns which are subject to reuse during the game. Size of
    // the list should equal to @NUMBER_OF_COLUMN_PAIRS
    private List<Column.ColumnPair> columnPairsList = new ArrayList<>();
    // holds the touch pad, which is equivalent to a full screen relative layout
    private View touchPad;
    // holds the text view for displaying the current score
    private TextView scoreTextView;
    // holds the text view for displaying the high score
    private TextView highScoreTextView;
    // holds the start button which can ignite the game by clicking it
    private Button startButton;

    // handler for handling ball display in every frame
    private Handler ballAnimationHandler = new Handler();
    // handlers for handling column pairs display in every frame. Size of the list should equal
    // to @NUMBER_OF_COLUMN_PAIRS (one handler for one pair of columns)
    private List<Handler> columnPairsAnimationHandlerList = new ArrayList<>();
    // runnable for @ballAnimationHandler to run
    private Runnable ballAnimationRunnable;
    // runnables for each handler in @columnPairsAnimationHandlerList to run
    private List<Runnable> columnPairsAnimationRunnableList = new ArrayList<>();
    // handles several utility tasks including jobs to do after ball crash, manipulation of score, etc.
    private Handler utilHandler = new Handler() {
        private int score = 0;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                /* handle a series of jobs after ball has just crashed a column */
                case FixedParam.BALL_CRASH:
                    // stop the columns from moving immediately
                    MainActivity.this.isColumnMoving = false;
                    // the player cannot make the ball jump anymore
                    MainActivity.this.touchPad.setOnTouchListener(null);
                    // record and set high score if there is a new one
                    if (SharedPreferencesUtil.getSharedPreferencesHighestScore(MainActivity.this) < score) {
                        SharedPreferencesUtil.setSharedPreferencesHighestScore(MainActivity.this, score);
                        MainActivity.this.highScoreTextView.setText("High Score: " + score);

                    }
                    // do some additional end game jobs before letting players replay
                    MainActivity.this.endGame();
                    // make a lightening flash to signal the ball has crashed
                    flashBackgroundOnce(MainActivity.this.touchPad);
                    // reset @score to 0 for next game cycle using the same handler;
                    this.score = 0;
                    break;

                /* add score every time the ball has safely passed a pair of columns  */
                case FixedParam.ADD_SCORE:
                    score++;
                    MainActivity.this.scoreTextView.setText("Score: " + Integer.toString(score));
                    break;

                /* make the start button visible after ball crushes and floats to the top of screen */
                case FixedParam.START_BUTTON_VISIBLE:
                    MainActivity.this.startButton.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
        }
    };
        // flag indicating and signaling if the columns should move or not. Initial state = false.
        private boolean isColumnMoving = false;
        // flag indicating and signaling if the game is started and running or not.
        // Initial state = false.
        private boolean isGameRunning = false;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_main);

            this.touchPad = findViewById(R.id.relative_layout_activity_main);

            this.startButton = findViewById(R.id.button_start);
            // start button listens to click event
            this.startButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setVisibility(View.INVISIBLE);
                    MainActivity.this.startGame();
                }
            });

            final View ballView = findViewById(R.id.view_activity_main_ball);
            // initialize the ball with its top = ControllableParam@INIT_BALL_TOP, left =
            // ControllableParam@INIT_BALL_LEFT, and the time parameter to be one frame before
            // the ball passing through its trough, so the game can start to run with first time
            // frame of time parameter 500, which represents the moment when the ball is at the
            // trough
            this.ball = new Ball(ballView, ControllableParam.INIT_BALL_TOP,
                    ControllableParam.INIT_BALL_LEFT, 500 - ControllableParam.BALL_MOVING_SPEED);
            // initialize ball height and width
            initBallHeightAndWidth(this.ball);
            this.ballAnimationRunnable = new Runnable() {
                @Override
                public void run() {
                    // the ball will only get continuously animated if the flag @isGameRunning is
                    // set to be true (this holds between the game gets started and after the ball
                    // crashes and just before the @startButton becomes visible again
                    if (MainActivity.this.isGameRunning) {
                        clipDrawableBallAnimation(MainActivity.this.ball,
                                MainActivity.this.ballAnimationHandler,
                                this);
                    }
                }
            };

            // initialize columns, number of pair of them = @NUMBER_OF_COLUMN_PAIRS
            for (int i = 0; i < NUMBER_OF_COLUMN_PAIRS; i++) {
                // each column view is instantiated dynamically not by using xml
                View topColumnView = new View(this);
                View bottomColumnView = new View(this);

                ((RelativeLayout)this.touchPad).addView(topColumnView);
                ((RelativeLayout)this.touchPad).addView(bottomColumnView);

                Column topColumn = new Column(topColumnView);
                Column bottomColumn = new Column(bottomColumnView);

                final Column.ColumnPair columnPair = new Column.ColumnPair(topColumn, bottomColumn);
                // set index of each column pair to be the current loop number
                columnPair.setIndex(i);
                this.columnPairsList.add(columnPair);

                // instantiate handlers for each column pair
                final Handler columnPairsAnimationHandler = new Handler();
                this.columnPairsAnimationHandlerList.add(columnPairsAnimationHandler);

                Runnable columnPairsAnimationRunnable = new Runnable() {
                    @Override
                    public void run() {
                        // the columns will only get continuously animated if the flag
                        // @isColumnMoving is set to be true (this holds between the game gets
                        // started and just after the ball crashes
                        if (MainActivity.this.isColumnMoving)
                            clipDrawableColumnPairAnimation(columnPair, columnPairsAnimationHandler, this);
                    }
                };
                this.columnPairsAnimationRunnableList.add(columnPairsAnimationRunnable);
            }

            // initialize height and width for each column
            initColumnHeightAndWidth(this.columnPairsList);
            // set the ball and all columns to be invisible before the game ignites
            setBallAndColumnsVisibility(this.ball, this.columnPairsList, false);
        }

    @Override
    protected void onStart() {
        super.onStart();

        this.scoreTextView = findViewById(R.id.text_view_score_board);
        // set current score to 0 before game starts
        this.scoreTextView.setText("Score: 0");
        // bring current score board to front of screen
        this.scoreTextView.bringToFront();

        this.highScoreTextView = findViewById(R.id.text_view_high_score_board);
        // set high score to the value stored in SP storage previously before game starts
        this.highScoreTextView.setText("High Score: " + SharedPreferencesUtil.getSharedPreferencesHighestScore(this));
        // bring high score board to front of screen
        this.highScoreTextView.bringToFront();
    }

    @Override
    protected void onResume() {
            super.onResume();
            // make the display to be fullscreen without action bar and status bar
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
    }

    /* method to handle a series of jobs just after every time the game has started running */
    private void startGame() {
        // reset the current score board to 0
        this.scoreTextView.setText("Score: 0");
        // reset the left positions of all column pairs to be out of the screen
        initColumnLeft(this.columnPairsList);
        // reset the background color of all column pairs (transparent-black and transparent-white
        // alternately
        initColumnsBackgroundColor(this.columnPairsList);
        // set to be true to signal columns start moving
        this.isColumnMoving = true;
        // set to be true to signal game starts running (mainly to signal the ball to move)
        this.isGameRunning = true;
        for (int i = 0; i < NUMBER_OF_COLUMN_PAIRS; i++) {
            // set the offset of all columns from the top of screen randomly
            initColumnsTopOffset(this.columnPairsList.get(i));
            // get all column handlers to start running the runnables, so all columns can start to
            // animate in subsequent time frames
            this.columnPairsAnimationHandlerList.get(i).post(this.columnPairsAnimationRunnableList.get(i));
        }

        // set the view of ball to be in the front of screen (so the ball view will be in front of
        // all views eventually)
        this.ball.bringToFront();
        // reset the time parameter to be one frame before the ball passing through its trough, so
        // the game can start to run with first time frame of time parameter 500, which represents
        // the moment when the ball is at the trough
        this.ball.setTrajectoryTime(500 - ControllableParam.BALL_MOVING_SPEED);
        // reset the the offset of "ground" of trajectory from the top of screen to be 0 (i.e.
        // exactly the top of screen)
        this.ball.setTopOffSet(0);
        //set the Ball@nextTop and Ball@top to be ControllableParam@INIT_BALL_TOP
        this.ball.setNextTop(ControllableParam.INIT_BALL_TOP);
        this.ball.setTopFromNextTop();
        // get the ball handler to start running the runnable, so the ball can start to
        // animate in subsequent time frames
        this.ballAnimationHandler.post(this.ballAnimationRunnable);

        // the player can start making the ball jump
        this.touchPad.setOnTouchListener(this);
        // set the ball and all columns to be visible
        setBallAndColumnsVisibility(this.ball, this.columnPairsList, true);

        // this thread is for checking if the ball has crashed, and will continue to run until
        // the ball really crashes
        new Thread() {

            // this list records the index of column pairs which has the ball currently passing
            // through them (usually there will only be just one pair having the ball passing
            // through, but if the adjacent column pairs are set the get much closer or the ball
            // width is set to be wider, then there may be more than one pair to have the ball
            // passing through in the same time
            private List<Integer> focusColumnList = new ArrayList<>();

            // flag showing the ball crashes or not, initialized to be false
            private boolean ballCrashed = false;

            public void run() {
                // keep looping to check if ball is crashed until the ball is really crashed
                // (signalled by the @ballCrashed flag)
                while (!ballCrashed) {
                    for (int i = 0; i < MainActivity.this.columnPairsList.size(); i++) {
                        // check if any column pair is having the ball passing through it
                        if (MainActivity.this.columnPairsList.get(i).getTopColumn().getLeft()
                                > (MainActivity.this.ball.getLeft() - ControllableParam.COLUMN_WIDTH)
                                && MainActivity.this.columnPairsList.get(i).getTopColumn().getLeft()
                                < (MainActivity.this.ball.getLeft() + ControllableParam.BALL_WIDTH)) {

                            // if the current column pair is having the ball passing through it but
                            // is not yet added to the @focusColumnList, then add its index
                            // to the list
                            if (!focusColumnList.contains(i))
                                focusColumnList.add(i);
                        } else {
                            // if the current column pair is not having the ball passing through it,
                            // but is still having its index inside the @focusColumnList (usually
                            // happens just after the ball has finished passing through the column
                            // pair), then remove its index from the list
                            if (focusColumnList.contains(i)) {
                                focusColumnList.remove((Integer)i);
                                // add current score by 1 to indicate the ball has successfully
                                // fully passed through a column pair
                                MainActivity.this.utilHandler.sendEmptyMessage(FixedParam.ADD_SCORE);
                            }
                        }
                    }

                    synchronized (MainActivity.this.ball) {
                        // to check if any column pair is in the @focusColumnList
                        if (!focusColumnList.isEmpty()) {
                            // to check if the ball has crashed for every column pair in the list
                            for (Integer index : focusColumnList) {
                                // the ball is crashed if its full body is not contained in the
                                // gap area between up and bottom columns
                                if (MainActivity.this.ball.getTop()
                                        < (MainActivity.this.columnPairsList.get(index).getBottomColumn().getTop()
                                        - ControllableParam.VERTICAL_COLUMN_SPACING)
                                        || MainActivity.this.ball.getTop() + ControllableParam.BALL_HEIGHT
                                        > (MainActivity.this.columnPairsList.get(index).getBottomColumn().getTop())) {
                                    // ignite a series of jobs for ball crash
                                    MainActivity.this.utilHandler.sendEmptyMessage(FixedParam.BALL_CRASH);
                                    // set the flag to be true to halt the while loop
                                    ballCrashed = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }.start();

    }

    // this method will do some end game jobs before letting players replay, the thread should run
    // just after the ball crashes until the start button is reloaded
    private void endGame() {
        //
        new Thread() {

            @Override
            public void run(){

                // do nothing after the ball crashes until the ball floats to the top of screen
                while(MainActivity.this.ball.getTop() > 0) {}

                // make the start button visible again for player to restart the game
                MainActivity.this.utilHandler.sendEmptyMessage(FixedParam.START_BUTTON_VISIBLE);
                // set the flag to false to signal the ball handler and runnable to end ball
                // animation
                MainActivity.this.isGameRunning = false;

            }
        }.start();

    }

    /* Tis Activity class implements OnTouchListener interface for detecting players touching
       the screen in order to let the ball jump during the game running. */
    public boolean onTouch(View view, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                synchronized (this.ball) {
                    // A jump indicates the ball is going to follow a new trajectory. The time
                    // parameter in current frame is reset to be ControllableParam@BALL_JUMP_TIME
                    // in order to start such a new trajectory. This value should be significantly
                    // smaller than 500 (the trough value), or otherwise the ball cannot jump and
                    // will float continuously even the player touches the screen frequently
                    this.ball.setTrajectoryTime(ControllableParam.BALL_JUMP_TIME);
                    // reset the "ground" level of the new trajectory
                    this.ball.setTopOffSet(this.ball.getTop()
                            - displacementCal(ControllableParam.BALL_JUMP_TIME));
                    // set the top position of ball in the next time frame
                    this.ball.setNextTop(ball.getTopOffSet() + displacementCal(ball.getTrajectoryTime() + ControllableParam.BALL_MOVING_SPEED));
                }
                break;
            default:
                break;
        }
        return true;
    }


    /* * * * * * * * * * * * * * * * * * *
     *  Static Utility Private Methods   *
     * * * * * * * * * * * * * * * * * * */
    
    /* To set the animation details of ball in current time frame */
    private static void clipDrawableBallAnimation(Ball ball, Handler handler, Runnable runnable) {

        synchronized (ball) {
            // update the time parameter for current time frame
            ball.setTrajectoryTime(ball.getTrajectoryTime() + ControllableParam.BALL_MOVING_SPEED);
            // this is to ensure the top position of ball would never get higher than the top of 
            // screen, so the ball would rest on the top of screen unless it is ignited to jump
            if (ball.getNextTop() <= 0)
                ball.setNextTop(0);

            ball.setTopFromNextTop();
            // set the top position of ball for next frame
            ball.setNextTop(ball.getTopOffSet() + displacementCal(ball.getTrajectoryTime() + ControllableParam.BALL_MOVING_SPEED));
        }
        // wait until the next time frame to ignite the ball animation
        handler.postDelayed(runnable, ControllableParam.ANIMATION_FRAME_DURATION);
    }
    
    /* To set the animation details of column pair in current time frame */
    private static void clipDrawableColumnPairAnimation(Column.ColumnPair columnPair, Handler handler, Runnable runnable) {

        // update left position of the pair from Column@nextLeft for current time frame
        columnPair.getTopColumn().setLeftFromNextLeft();
        columnPair.getBottomColumn().setLeftFromNextLeft();

        // set left position of the pair in Column@nextLeft for the next frame (moving to the left
        // at constant speed)
        columnPair.getTopColumn().setNextLeft(columnPair.getTopColumn().getLeft() - ControllableParam.COLUMN_MOVING_SPEED);
        columnPair.getBottomColumn().setNextLeft(columnPair.getBottomColumn().getLeft() - ControllableParam.COLUMN_MOVING_SPEED);

        // check whether the left position of the pair reaches the far left offset. All column
        // pairs are moving to the left at constant speed in streamline, each is separated with 
        // a spacing ControllableParam@HORIZONTAL_COLUMN_SPACING. This check is for the sake of
        // reusing the column pair in order to achieve seamless motion of column pairs 
        if (columnPair.getTopColumn().getLeft() <= FixedParam.SCREEN_WIDTH
                - ControllableParam.HORIZONTAL_COLUMN_SPACING * NUMBER_OF_COLUMN_PAIRS) {
            // reset the left position of column pairs to be at the right of screen for the next 
            // frame
            columnPair.getTopColumn().setNextLeft(FixedParam.SCREEN_WIDTH);
            columnPair.getBottomColumn().setNextLeft(FixedParam.SCREEN_WIDTH);
            // set the top position of the pair with a new random number
            initColumnsTopOffset(columnPair);
            // this is to make sure the color of the column pairs keep to be in alternate
            // transparent-black and transparent-white
            if (columnPair.getBackgroundColor() == MyApp.getContext().getResources().getColor(R.color.transparent_white))
                columnPair.setBackgroundColor(MyApp.getContext().getResources().getColor(R.color.transparent_black));
            else
                columnPair.setBackgroundColor(MyApp.getContext().getResources().getColor(R.color.transparent_white));
        }

        // wait until the next time frame to ignite the column pair animation
        handler.postDelayed(runnable, ControllableParam.ANIMATION_FRAME_DURATION);
    }

    /* Initialize height and width of the ball. */
    private static void initBallHeightAndWidth(Ball ball) {
        ball.setHeightAndWidth((int)ControllableParam.BALL_WIDTH, (int)ControllableParam.BALL_WIDTH);
    }

    /* Initialize height and width of all columns. Default height is height of screen. */
    private static void initColumnHeightAndWidth(List<Column.ColumnPair> columnPairsList) {
        for (Column.ColumnPair columnPair: columnPairsList)
            columnPair.setHeightAndWidth(FixedParam.SCREEN_HEIGHT, (int)ControllableParam.COLUMN_WIDTH);
    }

    /* Initialize offset of a pair of columns from top of screen. */
    private static void initColumnsTopOffset(Column.ColumnPair columnPair) {
        Column topColumn = columnPair.getTopColumn();
        Column bottomColumn = columnPair.getBottomColumn();
        // set the top position of the top column with columnTopOffsetRandomGenerator()
        topColumn.setTop(columnTopOffsetRandomGenerator());
        // set the bottom position of the bottom column with reference to the top position of the
        // just set top column
        bottomColumn.setTop(topColumn.getTop() + (float)FixedParam.SCREEN_HEIGHT
                + ControllableParam.VERTICAL_COLUMN_SPACING);
    }

    /* Initialize left position of all "queuing" column pairs with the first one to be screen width,
       and the followings to be screen width plus multiples of column width , so that all columns
       are ready to appear successively from right of screen after game starts. */
    private static void initColumnLeft(List<Column.ColumnPair> columnPairsList) {
        float screenWidth = (float)FixedParam.SCREEN_WIDTH;
        for (int i = 0; i < columnPairsList.size(); i++) {
            Column.ColumnPair columnPair = columnPairsList.get(i);
            columnPair.getTopColumn().setNextLeft(screenWidth
                    + i * ControllableParam.HORIZONTAL_COLUMN_SPACING);
            columnPair.getTopColumn().setLeftFromNextLeft();
            columnPair.getBottomColumn().setNextLeft(screenWidth
                    + i * ControllableParam.HORIZONTAL_COLUMN_SPACING);
            columnPair.getBottomColumn().setLeftFromNextLeft();

        }
    }
    
    /* initialize color of column pairs in alternate transparent-white and transparent-black */
    private static void initColumnsBackgroundColor(List<Column.ColumnPair> columnPairsList) {
        for (Column.ColumnPair columnPair: columnPairsList) {
            if (columnPair.getIndex() % 2 == 0)
                columnPair.setBackgroundColor(MyApp.getContext().getResources().getColor(R.color.transparent_white));
            else
                columnPair.setBackgroundColor(MyApp.getContext().getResources().getColor(R.color.transparent_black));
        }
    }

    /* static method controlling the ball and all columns visible or not */
    private static void setBallAndColumnsVisibility(Ball ball, List<Column.ColumnPair> columnPairsList, boolean visible) {
        ball.setVisibility(visible);
        for (Column.ColumnPair columnPair: columnPairsList)
            columnPair.setVisibility(visible);
    }

    /* The inverse (vertical) trajectory of the ball is designed to pass through an offset from top
       of screen defined by its Ball@topOffset when its Ball@ballTime = 0 or 1000 (the two
       roots of a parabola). This method returns the vertical displacement of the ball from
       Ball@topOffset at Ball@trajectoryTime for a pre-set trough from {@topOffset} to 
       ControllableParam@BALL_TROUGH. */
    private static float displacementCal(float time) {
        float t = time / 1000;
        float h = ControllableParam.BALL_TROUGH;
        return 4*h*t*(1-t);
    }

    /* Static method for generating a random number for offset of every top column from the top
       of screen, which is a negative number and represents a proper fraction of column height
       (column height is equal to screen height by default). */
    private static float columnTopOffsetRandomGenerator() {
        final int screenHeight = FixedParam.SCREEN_HEIGHT;
        return (float) (ControllableParam.BOTTOM_COLUMN_MAX_TOP_OFFSET
                - ControllableParam.VERTICAL_COLUMN_SPACING - screenHeight
                - Math.random() * (ControllableParam.BOTTOM_COLUMN_MAX_TOP_OFFSET
                - ControllableParam.VERTICAL_COLUMN_SPACING - ControllableParam.TOP_COLUMN_MIN_BOTTOM_OFFSET));
    }

    /* static method for the input view (usually the whole screen) to flash once with duration
       = 100 ms, with flash background to be transparent-white */
    private static void flashBackgroundOnce(View viewToBeFlashed) {
        final View view = viewToBeFlashed;
        view.getBackground().setColorFilter(
                MyApp.getContext().getResources().getColor(
                        R.color.transparent_white),
                PorterDuff.Mode.SRC_ATOP);
        view.invalidate();

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                view.getBackground().clearColorFilter();
                view.invalidate();
            }
        },100);
    }
}
